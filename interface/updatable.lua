---@class Updatable
local Updatable = {}

---@param dt number
function Updatable:update(dt) end

return Updatable