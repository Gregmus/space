local Scene = require('scenes.scene')
local Color = require('color')
local DrawObject = require('game_object.draw_object')
local Event = require('enum.event')
local Rectangle = require('drawable.rectangle')

---@class LoadScene : Scene
local LoadScene = Scene:new()

---@param prevScene Scene
function LoadScene:load(prevScene)
    if self.isLoaded then
        return
    end
    self.isLoaded = true

    local rect = Rectangle:new('fill', Color:white(), 50, 100)
    local gameObject1 = DrawObject:new(rect, wpixels(4.5), App.camera.y)
    local gameObject2 = DrawObject:new(rect, wpixels(5.5), App.camera.y)

    self:addVisible(gameObject1)
    self:addVisible(gameObject2)

    self.events:addAction(Event.KEY, function() App.changeScene(prevScene) end, 'space')
end

return LoadScene
